<?php

namespace Drupal\uw_cbl_mailman\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblMailmanAlterForm.
 */
class UwCblMailmanAlterForm extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    if ($this->checkLayoutBuilder($event, 'Mailman')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for blockquote.
      $form['#validate'][] = [$this, 'uw_cbl_mailman_validation'];
    }
  }

  /**
   * Form validation for mailman.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function uw_cbl_mailman_validation(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        // If there is no mailman server selected, set error.
        if ($block['field_uw_mm_server'][0]['value'] == '') {

          // Set the form error for not having a mailman server.
          $form_state->setErrorByName('settings][block_form][field_uw_mm_server', 'You must select a mailman server.');
        }
        // If there is no mailman mailing list subscription URL entered, set error.
        else if ($block['field_uw_mm_servername'][0]['value'] == '') {

          // Set the form error for not having mailman mailing list subscription URL.
          $form_state->setErrorByName('settings][block_form][field_uw_mm_servername', 'You must enter mailman mailing list subscription URL.');
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HookEventDispatcherInterface::FORM_ALTER => 'alterForm',
    ];
  }
}
