<?php

namespace Drupal\uw_cbl_mailman\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblMailmanPreprocessBlock.
 */
class UwCblMailmanPreprocessBlock extends UwCblBase implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Preprocess blocks with mailman and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_mailman')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block
      $block = $variables->getByReference('content')['#block_content'];

      // Set the variables for the mailman.
      $mailman = [
        'server' => $block->field_uw_mm_server->value,
        'servername' => $block->field_uw_mm_servername->value,
        'uniqueid' => $block->uuid(),
      ];

      // Set the mailman in variables.
      $variables->set('mailman', $mailman);
    }
  }
}
